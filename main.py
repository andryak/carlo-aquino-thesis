from collections import defaultdict
from csv import DictReader
from datetime import date, datetime
from itertools import combinations, groupby, tee
from operator import itemgetter
from statsmodels.tsa.stattools import grangercausalitytests


class YearRange(object):
    def __init__(self, start, end):
        """Create a new time range from `start` year to `end` year."""
        assert(start <= end)
        self.start = start
        self.end = end
        self.start_date = date(year=start, month=1, day=1)
        self.end_date = date(year=end, month=12, day=31)

    def __contains__(self, item):
        return self.start_date <= item <= self.end_date

    def __hash__(self):
        return hash((self.start, self.end))

    def __eq__(self, other):
        return isinstance(other, YearRange) and self.start == other.start and self.end == other.end


def pairwise(iterable):
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def momentum(current, last):
    return (current - last) / last


def momentums(values):
    return [momentum(*pair) for pair in pairwise(values)]


def parse_value(value):
    return float(value) if value else None


def aggregate_monthly(ts):
    return [max(group, key=itemgetter(0)) for month, group in groupby(ts, key=lambda pair: pair[0].month)]


def granger_test_pvalue(l1, l2, maxlag, test='ssr_chi2test'):
    return grangercausalitytests(list(zip(l1, l2)), maxlag=maxlag, verbose=False)[maxlag][0][test][1]


if __name__ == '__main__':
    # Read all time series from a CSV file, parse them and store them into a map.
    TIME_SERIES_FILE = 'data/time_series.csv'

    underlying_to_ts = defaultdict(list)
    with open(TIME_SERIES_FILE, newline='') as csv_file:
        reader = DictReader(csv_file)
        for row in reader:
            date_str = row.pop('date')
            date_ = datetime.strptime(date_str, '%m/%d/%Y').date()
            for underlying, value in row.items():
                underlying_to_ts[underlying].append((date_, parse_value(value)))

    # Remove underlyings for which we do not have enough data.
    del underlying_to_ts['AT0000A0K1J1']
    del underlying_to_ts['AT0000BAWAG2']

    # For each underlying, drop all observations prior to 2004-01-01 and after 2018-12-31.
    underlying_to_ts = {
        underlying: [pair for pair in ts if pair[0] in YearRange(2004, 2018)]
        for underlying, ts in underlying_to_ts.items()
    }

    underlying_to_ts = {
        underlying: aggregate_monthly(ts)
        for underlying, ts in underlying_to_ts.items()
    }

    # We have no observation for Raffaisen bank until 2005-04-29, we set
    # observations prior to that date to the value registered for 2005-04-29.
    RAFFAISEN = 'AT0000606306'
    underlying_to_ts[RAFFAISEN] = [
        (date_, value) if value is not None else (date_, 38.305)
        for date_, value in underlying_to_ts[RAFFAISEN]
    ]

    # Focus on blocks of three years around the financial crisis (2007 - 2009) and calculate momentums in such periods.
    periods = [
        YearRange(2004, 2006),
        YearRange(2007, 2009),
        YearRange(2010, 2012),
        YearRange(2013, 2015),
    ]

    period_to_underlying_to_momentum = defaultdict(dict)
    for period in periods:
        for underlying, ts in underlying_to_ts.items():
            period_ts = [pair for pair in ts if pair[0] in period]
            period_ts.sort(key=itemgetter(0))
            period_to_underlying_to_momentum[period][underlying] = momentums([value for date_, value in period_ts])

    # For each pair of underlyings, determine if their momentum data in each period of interest Granger-cause each other.
    # We use maxlag=3 and 5% as the pvalue threshold for the Granger test.
    MAXLAG = 3
    PVALUE = 0.05
    period_to_table = defaultdict(lambda: defaultdict(dict))
    for period in periods:
        underlying_to_momentum = period_to_underlying_to_momentum[period]
        for (underlying1, values1), (underlying2, values2) in combinations(underlying_to_momentum.items(), 2):
            dir_correlation = granger_test_pvalue(values1, values2, maxlag=MAXLAG)
            inv_correlation = granger_test_pvalue(values2, values1, maxlag=MAXLAG)
            period_to_table[period][underlying1][underlying2] = int(dir_correlation < PVALUE)
            period_to_table[period][underlying2][underlying1] = int(inv_correlation < PVALUE)

    # Convert the computed data to CSV format and print it to stdout.
    ISINS = [
        # Banks.
        'AT0000652011',
        'AT0000606306',
        'AT0000625538',
        'AT0000625504',
        'AT0000624739',
        'AT0000624705',
        'AT0000625132',
        'AT0000625108',
        'AT0000824701',
        'AT0000741301',

        # Insurances.
        'AT0000821103',
        'AT0000908504',
    ]

    for period in periods:
        print('from:', period.start, sep=',')
        print('to:', period.end, sep=',')
        print('', ','.join(ISINS), sep=',')
        table = period_to_table[period]
        for isin1 in ISINS:
            print(isin1, end=',')
            formatted_values = [table[isin1][isin2] if isin1 != isin2 else '' for isin2 in ISINS]
            print(','.join(map(str, formatted_values)))
        print()
